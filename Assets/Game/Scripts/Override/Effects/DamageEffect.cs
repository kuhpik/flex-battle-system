﻿using UnityEngine;
using UnityEditor;
using System.Linq;

public class DamageEffect : Effect
{
    protected override void Impact()
    {
        var damage = 0f;

        if (overrided != null) // can be any stat
        {
            if (overrided.Length > 2)
                throw new System.Exception("For damage effect you should pass min and max dmg");

            overrided = overrided.OrderBy(x => x.value).ToArray();
            damage = Random.Range(overrided[0].value, overrided[1].value + 1);
        }

        else
        {
            damage = Random.Range(caster.GetData<Stat>("Damage").Value, caster.GetData<Stat>("MaxDamage").Value + 1);
        }

        statsChanged = damage;
    }
}