﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct StatApplyEffectData
{
    /// <summary>
    /// Which stat needs for apply\clear effect
    /// </summary>
    public Stat stat;

    /// <summary>
    /// Needed stat value
    /// </summary>
    public float value;

    /// <summary>
    /// Chance to apply
    /// </summary>
    public float chance;

    /// <summary>
    /// Effect active when value is lower than this value?
    /// </summary>
    public bool isLowerThan;//Here can be more conditions but well

    /// <summary>
    /// True to apply, false to remove
    /// </summary>
    public bool isApply;
}
