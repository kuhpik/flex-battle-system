﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct StatData
{
    /// <summary>
    /// Which stat will be affected
    /// </summary>
    public Stat stat;

    /// <summary>
    /// Also can be used for a formula (how much do we get by one stat)
    /// </summary>
    public float value;
}
