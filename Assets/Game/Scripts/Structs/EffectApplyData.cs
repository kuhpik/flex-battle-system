﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct EffectApplyData
{
    /// <summary>
    /// Which effect needs for apply\clear effect
    /// </summary>
    public Effect stat;

    /// <summary>
    /// Chance to apply effect
    /// </summary>
    public float chance;

    /// <summary>
    /// True to apply, false to remove
    /// </summary>
    public bool isApply;
}
