﻿using UnityEngine;
using UnityEditor;

public enum WeaponType
{
    Cold,
    Shooting,
    Fire
}

public enum WeaponRangeType
{
    Melee,
    Range
}