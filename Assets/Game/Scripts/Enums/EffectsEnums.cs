﻿using UnityEngine;
using UnityEditor;

public enum RangeType
{
   First,
   Last,
   Any,
   NOTFirst,
   NOTLast
}

public enum AreaType
{
    Single,
    Area,
    Line,
    Collum,
    FullLine,
    FullCollum,
    FullArea
}

public enum TargetType
{
    Any,
    Ally,
    Enemy
}