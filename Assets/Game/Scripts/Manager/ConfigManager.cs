﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class ConfigManager : MonoBehaviour
{
    [SerializeField] private StatConfig[] statConfigs;
    [SerializeField] private UnitConfig[] unitConfigs;

    private Dictionary<System.Type, Dictionary<string, LocalizableConfig>> data;

    public void Init()
    {
        data = new Dictionary<System.Type, Dictionary<string, LocalizableConfig>>();

        data.Add(typeof(StatConfig), new Dictionary<string, LocalizableConfig>());
        data.Add(typeof(StatConfig), new Dictionary<string, LocalizableConfig>());
        data.Add(typeof(StatConfig), new Dictionary<string, LocalizableConfig>());
        data.Add(typeof(StatConfig), new Dictionary<string, LocalizableConfig>());
        data.Add(typeof(StatConfig), new Dictionary<string, LocalizableConfig>());

        foreach (var stat in statConfigs)
        {
            data[typeof(StatConfig)].Add(stat.Stat.GetID(), stat);
        }        
    }

    public IEnumerable<T> GetData<T>() where T : LocalizableConfig
    {
        throw new System.NotImplementedException();
    }

    public T GetData<T>(string id) where T : LocalizableConfig
    {
        return (T)data[typeof(T)][id];
    }
}