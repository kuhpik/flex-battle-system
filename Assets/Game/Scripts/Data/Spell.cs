﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : Data
{
    [SerializeField] protected TargetType allowedTarget;
    [SerializeField] protected RangeType useFrom;
    [SerializeField] protected RangeType useTo;
}
