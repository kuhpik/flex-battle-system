﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : Data
{
    [SerializeField] protected Effect additionalEffect;
    [SerializeField] protected StatData[] stats;
}
