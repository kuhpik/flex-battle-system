﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Unit : Data
{
    [SerializeField] private StatData[] stats;
    [SerializeField] private Effect[] effects;
    [SerializeField] private Item[] items;
    [SerializeField] private Spell[] spells;
    [SerializeField] private Weapon weapon;

    private Dictionary<System.Type, Dictionary<string, Data>> data;

    public void Init()
    {
        data = new Dictionary<System.Type, Dictionary<string, Data>>();

        data[typeof(Stat)] = new Dictionary<string, Data>();
        data[typeof(Effect)] = new Dictionary<string, Data>();
        data[typeof(Item)] = new Dictionary<string, Data>();
        data[typeof(Spell)] = new Dictionary<string, Data>();

        foreach(var stat in stats)
        {
            var newStat = Instantiate(stat.stat);
            newStat.SetValue(stat.value);

            data[typeof(Stat)].Add(stat.stat.name, newStat);
        }

        foreach (var effect in effects)
        {
            data[typeof(Effect)].Add(effect.GetID(), effect);
        }

        foreach (var item in items)
        {
            data[typeof(Item)].Add(item.GetID(), item);
        }

        foreach (var spell in spells)
        {
            data[typeof(Spell)].Add(spell.GetID(), spell);
        }
    }

    public IEnumerable<T> GetData<T>() where T : Data
    {
        return data[typeof(T)].Select(x => x.Value).AsEnumerable() as IEnumerable<T>;
    }

    public T GetData<T>(string id) where T : Data
    {
        return data[typeof(T)][id] as T;
    }

    public Weapon GetWeapon()
    {
        return weapon;
    }

    public bool HasEffect(string name)
    {
        return data[typeof(Effect)].Select(x => (x.Value as Effect).name).Contains(name);
    }
}
