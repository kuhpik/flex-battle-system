﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data/Effect")]
public class Effect : Data
{
    [Header("Base")]
    [SerializeField] private bool shouldReturnStats;
    [SerializeField] private bool isInstant;
    [SerializeField] private int time;
    [SerializeField] StatData[] statsToAffect;

    [Header("Apply")]
    [SerializeField] private EffectApplyData[] effectApplyData;
    [SerializeField] private StatApplyEffectData[] statApplyEffectData;

    /// <summary>
    /// How many round effect can be active. -1 for endless
    /// </summary>
    public int Time => time;
    /// <summary>
    /// Check is stats should be returned back (like buff that gives +2 damage while active)
    /// </summary>
    public bool ShouldReturnStats => shouldReturnStats;
    /// <summary>
    /// For effects like damage or healing
    /// </summary>
    public bool IsInstant => isInstant;
    public StatData[] StatsToAffect => statsToAffect;
    public EffectApplyData[] EffectApplyData => effectApplyData;
    public StatApplyEffectData[] StatApplyEffectData => statApplyEffectData;

    /// <summary>
    /// Used to right math calculations and debugging
    /// </summary>
    protected float statsChanged;
    /// <summary>
    /// In case if we want re-stackable effects. Can be helpfull in balance
    /// </summary>
    protected int timetotal;
    protected Unit caster;
    protected Unit target;
    protected StatData[] overrided;

    public virtual void Apply(Unit caster, Unit target, StatData[] overrided = null)
    {
        this.caster = caster;
        this.target = target;
        if (overrided != null) this.overrided = overrided;

        if (caster.HasEffect("Dead"))
        {
            Debug.Log("Target is dead, can't apply effects");
            return;
        }

        if (isInstant)
        {
            Impact();
            Remove();
        }
    }

    public virtual void Tick()
    {
        Impact();

        time--;
        if (time == 0) Remove();
    }

    public virtual float Remove()
    {
        if (ShouldReturnStats)
        {
            foreach (var stat in statsToAffect)
            {

            }
        }
        return statsChanged;
    }

    protected virtual void Impact()
    {

    }
}