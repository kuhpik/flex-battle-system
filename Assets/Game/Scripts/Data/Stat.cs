﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Stat")]
public class Stat : Data
{
    [SerializeField] private float value;
    [SerializeField] private bool canBeMinus;

    public float Value => value;
    public bool CanBeMinus => canBeMinus;

    /// <summary>
    /// Pass positive or negative value
    /// </summary>
    /// <param name="value"></param>
    public void ChangeValue(float value)
    {
        this.value += value;
    }

    public void SetValue(float value)
    {
        this.value = value;
    }

    public override string GetID()
    {
        return name;
    }

    public override void SetID(string ID)
    {
        throw new System.Exception("Don't need to change id of a Stat");
    }
}
