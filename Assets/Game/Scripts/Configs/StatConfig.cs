﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class StatConfig : LocalizableConfig
{
    [SerializeField] private StatData[] statFormulas;
    [SerializeField] private Stat stat;

    public StatData[] StatFormulas => statFormulas;
    public Stat Stat => stat;
}