﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizableConfig : Config, ILocalizable
{
    [SerializeField] private string localizationKey;
    public string GetLocalizationKey() => localizationKey;
}
