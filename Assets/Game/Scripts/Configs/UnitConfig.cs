﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class UnitConfig : LocalizableConfig, IHaveIcon
{
    [SerializeField] private Unit unit;

    [Header("Graphics")]
    [SerializeField] private Sprite icon;
    [SerializeField] private Sprite view;

    [Header("Naming")]
    [SerializeField] private bool isUsingCustomNames;
    [SerializeField] private string[] names;

    public Unit Unit => unit;
    public Sprite Icon => icon;
    public Sprite View => view;
    public bool IsUsingCustomNames => isUsingCustomNames;
    public string[] Names => names;

    public string GetUnitName()
    {
        if (!isUsingCustomNames)
        {
            return names[Random.Range(0, names.Length)];
        }

        else
        {
            return "";
        }
    }
}