﻿using UnityEngine;
using UnityEditor;

public abstract class Data : ScriptableObject, IHaveID
{
    [SerializeField] private string ID;
    public virtual string GetID() { return ID; }
    public virtual void SetID(string ID) { this.ID = ID; }
}