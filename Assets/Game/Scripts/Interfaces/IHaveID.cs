﻿using UnityEngine;
using UnityEditor;

public interface IHaveID
{
    string GetID();
    void SetID(string ID);
}