﻿using UnityEngine;
using UnityEditor;

public interface IHaveIcon
{
    Sprite Icon { get; }
}