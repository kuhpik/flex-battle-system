﻿using UnityEngine;
using UnityEditor;

public interface ILocalizable
{
    string GetLocalizationKey();
}